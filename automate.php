<?php

class Commande {

	const DATAFILE = "data.json";
	protected $quantite;
	protected $choix;

	function __construct( $quantite, $choix ) {
		$this->quantite = $quantite;
		$this->choix = $choix;
	}

	function isQuantiteValid() {
		$value = $this->quantite;
		return ( is_numeric( $value ) and ( +$value > 0 ) and ( +$value <= 100 ) );
	}

	function isItemInRange() {
		$item = $this->choix;
		return ( is_numeric( $item ) and in_array( +$item, $range = self::getRange() ) );
	}

	function isValid() {
		return $this->isQuantiteValid() and $this->isItemInRange();
	}

	static function getRange() {

		if( !file_exists( self::DATAFILE ) ) {
			$range = self::random_array( 20, 0, 100 );
			sort( $range );
			file_put_contents( self::DATAFILE, json_encode( $range, JSON_PRETTY_PRINT ) );
		}
		else {
			$range = json_decode( file_get_contents( self::DATAFILE ), true );
		}
		return $range;
	}

	static function random_array( $length, $min, $max ) {
		$array = array();
		while( $length > 0 ) {
			$array[] = rand( $min, $max );
			$length--;
		}
		return $array;
	}
}

$quantite = isset( $_GET[ "quantite" ] ) ? $_GET[ "quantite" ] : null;
$choix = isset( $_GET[ "choix" ] ) ? $_GET[ "choix" ] : null;
$commande = new Commande( $quantite, $choix );

?>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
		  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Document</title>
</head>
<body>
	<form method="get" name="quantite">
		<h2>Automate de commande</h2>


		<label>
			<span>Quantité de produits (1 à 100): </span>
			<input type="text" name="quantite" value="<?php echo $quantite ?>">
		</label>
		<?php if( !is_null( $quantite ) and !$commande->isQuantiteValid() ): ?>
			<b>La quantité n'est pas correcte.</b>
		<?php endif; ?>


		<p>Liste des produits :
			<?php echo implode( ', ', Commande::getRange() ); ?>
		</p>
		<label>
			<span>Choix de produit: </span>
			<input type="text" name="choix" value="<?php echo $choix ?>">
		</label>
		<?php if( !is_null( $choix ) and !$commande->isItemInRange() ): ?>
			<b>Le choix n'est pas correct.</b>
		<?php endif; ?>


		<p>
			<input type="submit">
		</p>
	</form>

	<?php if( $commande->isValid() ): ?>
		<p>
			<b>La commande est valide.</b>
		</p>
	<?php endif; ?>
</body>
</html>

