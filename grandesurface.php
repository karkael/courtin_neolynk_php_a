<?php

// here, sha1 is used for reference creation.


/**
 * class Reference
 * is a basic class which allow user to create a reference.
 */

class Reference {

	const KEY_NAME = "name";
	const KEY_REFERENCE = "reference";
	const KEY_TYPE = "type";

	const TYPE_DENTIFRICE = "dentifrice";
	const TYPE_SHAMPOOING = "shampooing";
	//	...
	const TYPE_FARINE = "farine";


	protected $name;
	protected $reference;
	protected $type;


	function __construct( $name, $ref = null, $type ) {
		$this->name = $name;
		$this->reference = is_null( $ref ) ? sha1( $name ) : $ref;
		$this->type = $type;
	}

	function toArray() {
		return array(
			self::KEY_NAME => $this->name,
			self::KEY_REFERENCE => $this->reference,
			self::KEY_TYPE => $this->type
		);
	}

	function getType() {
		return $this->type;
	}

	static function instanceFromArray( $row ) {
		if( is_array( $row ) and isset( $row[ self::KEY_NAME ] ) and isset( $row[ self::KEY_REFERENCE ] ) and isset( $row[ self::KEY_TYPE ] ) ) {
			return new self(
				$row[ self::KEY_NAME ],
				$row[ self::KEY_REFERENCE ],
				$row[ self::KEY_TYPE ]
			);
		}
		else {
			throw new TypeError( "First parameter is not an array." );
		}
	}
}

class Dentifrices extends Reference {

	function __construct( $name, $ref = null ) {
		parent::__construct( $name, $ref, self::TYPE_DENTIFRICE );
	}

	// code spécifique aux Dentifrices
}

class Shampooings extends Reference {

	function __construct( $name, $ref = null ) {
		parent::__construct( $name, $ref, self::TYPE_SHAMPOOING );
	}

	// code spécifique aux Shampooings
}

// ...

class Farines extends Reference {

	function __construct( $name, $ref = null ) {
		parent::__construct( $name, $ref, self::TYPE_FARINE );
	}

	// code spécifique aux Farines
}




/**
 * class Catalogue
 * is a class which contains a list or references and which :
 * - can add a reference ;
 * - can filter references by type ;
 * - can return raw data.
 */

class Catalogue {

	protected $list = array();

	function __construct( $rowset = null ) {
		if( is_array( $rowset ) ) {
			foreach( $rowset as $row ) {
				if( is_array( $row ) ) {
					$this->addReference( Reference::instanceFromArray( $row ) );
				}
				elseif( $row instanceof Reference ) {
					$this->addReference( $row );
				}
				else {
					throw new TypeError( "Row is not an array nor a Reference instance." );
				}
			}
		}
		elseif( !is_null( $rowset )) {
			throw new TypeError( "First parameter is not an array." );
		}
	}

	function addReference( Reference $ref ) {
		$this->list[] = $ref;
	}

	function getReferencesByType( $type ) {
		$result = new self();
		foreach( $this->list as $ref ) {
			if( $ref instanceof Reference and ( $ref->getType() === $type )) {
				$result->addReference( $ref );
			}
		}
		return $result;
	}

	function toArray() {
		$result = array();
		foreach( $this->list as $ref ) {
			if( $ref instanceof Reference ) {
				$result[] = $ref->toArray();
			}
		}
		return $result;
	}
}


// RUN

// DATABASE EXTRACT, or hard reference creation
$sampleItemName = "Signal";
$selectAllFromReferencesTable = array(
	array(
		Reference::KEY_NAME => $sampleItemName,
		Reference::KEY_REFERENCE => sha1( $sampleItemName ),
		Reference::KEY_TYPE => Reference::TYPE_DENTIFRICE
	)
);


// ALL THE CATALOGUE
$mainCatalogue = new Catalogue( $selectAllFromReferencesTable );


// ADD ITEM IN THE CATALOGUE, soft reference creation
$mainCatalogue->addReference( new Shampooings( "Dop" ) );


// GET DENTIFRICE CATALOGUE
$dentifriceCatalogue = $mainCatalogue->getReferencesByType( Reference::TYPE_DENTIFRICE );


// PRINT DENTIFRICE CATALOGUE
header( "Content-Type: application/json; charset=utf-8" );
echo json_encode( $dentifriceCatalogue->toArray(), JSON_PRETTY_PRINT );
die();

